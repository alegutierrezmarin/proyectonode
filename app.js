const http = require('http');
const fs = require('fs');

const server = http.createServer((req, res) => {
    //console.log(req.url, req.method, req.headers);
    //process.exit();

    const url = req.url;
    const method = req.method;

    if (url === '/') {
        res.setHeader('Content-Type', 'text/html');
    // escribimos el codigo de la repuesta
        res.write('<html>');
        res.write('<head><title>Primera pagina con html</title></head>');
        res.write('<body><form action="/message" method="POST"><input type="text" name = "message"><button type="submit">Enviar</button></form></body>');
        res.write('</html>');
        
        return res.end();
    }
    if (url === '/message' && method === 'POST') {
        const body = [];
        req.on('data', (chunk) => {
            console.log(chunk);
            body.push(chunk);
        });
        return req.on('end', () => {
            const parseBody = Buffer.concat(body).toString();
            //console.log(parseBody);
            const message = parseBody.split('=')[1];
            fs.writeFileSync('message.txt', message, err => {
                res.statusCode = 302;
                res.setHeader('Location', '/');
                return res.end();    
            });
        });
    }
    
    res.setHeader('Content-Type', 'text/html');
    res.write('<html>');
    res.write('<head><title>Primera pagina con html</title></head>');
    res.write('<body><h1>Esta es la pagina de prueba inicial</h1></body>');
    res.write('</html>');
    res.end();

});
server.listen(3000)
